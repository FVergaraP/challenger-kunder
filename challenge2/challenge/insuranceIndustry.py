import requests,traceback,json
from datetime import date

URL_INSURANCE = 'https://hack.kunderlabs.com/exam/insurance/api/insurance'
URL_INSURANCE_hired_TODAY = 'https://hack.kunderlabs.com/exam/insurance/api/insurance/contracted/today'

class Insurance:

    def __init__(self,id_,name,codeType,type_,initDate,endDate):
        self.id_ = id_
        self.name = name
        self.codeType = codeType
        self.type_ = type_
        self.initDate = initDate
        self.endDate = endDate
        self.hired_today = 0

    def getMostHiredInsuranceLast24Hrs():

        list_insurance = Insurance.get_insurances()
        list_last_24hrs_insurance = Insurance.get_last_insurances_hired()
        
        Insurance.counter_hired(list_insurance,list_last_24hrs_insurance)
        
        list_insurance_sorted = sorted(list_insurance, key=lambda insurance: insurance.hired_today, reverse=True)
        top5_hired_today = list_insurance_sorted[:5]
        
        result_json = []
        for i in top5_hired_today:
            result_json.append(json.dumps(i.__dict__))
        return result_json

    def get_insurances():
        r = requests.get(URL_INSURANCE)
        r.raise_for_status()
        r_json = r.json()
        if 'insurance' in r_json and r_json['insurance']:
            list_insurance = []
            for insurance in r_json['insurance']:
                new_insurance = Insurance(insurance['id'],insurance['name'],insurance['codeType'],insurance['type'],insurance['initDate'],insurance['endDate'])
                list_insurance.append(new_insurance)
            return list_insurance
        else:
            raise Exception("ERROR: Doesn't exists insurances in the system")

    def get_last_insurances_hired():
        r = requests.get(URL_INSURANCE_hired_TODAY)
        r.raise_for_status()
        r_json = r.json()
        if 'contracted' in r_json and r_json['contracted']:
            if 'date' in r_json['contracted'] and r_json['contracted']['date']:
                current_date = date.today().strftime('%d-%m-%Y')
                if  r_json['contracted']['date'] == current_date:
                    if 'results' in r_json['contracted'] and r_json['contracted']['results']:
                        list_insurance_hired = r_json['contracted']['results']
                        return list_insurance_hired
                    else:
                        raise Exception("INFO: Doesn't exist insurances hired today")
                else:
                    raise Exception("ERROR: The system don't return current insurance")
            else:
                raise Exception("ERROR: We can't verify the date for last insurances hired")
        else:
            raise Exception("ERROR: Don't exists insurances hired in the system")

    def counter_hired(company_insurances,hired_insurances):
        if hired_insurances and company_insurances:
            for h_insurance in hired_insurances:
                for c_insurance in company_insurances:
                    if h_insurance['insuranceId'] == c_insurance.id_:
                        c_insurance.hired_today += 1
                        break


def main():
    try:
        top5_hired = Insurance.getMostHiredInsuranceLast24Hrs()
        print(top5_hired)
    except Exception as e:
        print(e)
main()