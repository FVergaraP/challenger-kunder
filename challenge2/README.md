# Desafío 2 Kunder
 
Segundo desafío para el proceso de postulación a desarrollador fullstack, que consiste en obtener el top 5 de seguros contratados en las últimas 24 hrs.
 
## Comenzando 🚀
 
Para obtener resultados válidos es importante que los servicios de la API de kunder esten UP, para que así obtener los 5 seguros más contratados en las últimas 24hrs.

Para realizar la revisión es necesario bajar el proyecto
 
```
git clone https://FVergaraP@bitbucket.org/FVergaraP/challenger-kunder.git
```
 
 
## Requisitos/Recomendaciones 📋
 
*Para asegurar una correcta ejecución se recomienda el uso de Python3.7
 
 
## Ejecución 🔧
 
Para obtener el resultado se debe ejecutar el scpryt insuranceIndustry.py
 
```
PATH/PYTHON/python3.7 /PATH/PROYECTO/insuranceIndustry.py
```
 
## Construido con 🛠️
 
* Python 3.7
* VisualStudioCode
* Postman
 
 
## Autor ✒️
 
**Francisco Vergara Peña** - *Desarrollador*