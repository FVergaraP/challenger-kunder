class Utils:

    def validate_args(args):
        if len(args) < 2:
            raise Exception("ERROR:Forgot the file path")
        elif len(args) > 2:
            print("WARNING: Exist 2 or more arguments, the first one will be used")
            return True

    def get_oposite_brack(brack):
        if brack == ')':
            return '('
        elif brack == ']':
            return '['
        elif brack == '}':
            return '{'
        else:
            raise Exception("ERROR: Incorrect brack '{}'".format(brack))

    def expression_to_list(n_expression):
        list_expression = []
        for idx,n in enumerate(n_expression):
            if n.isnumeric():                
                if idx >0 and n_expression[idx-1].isnumeric():
                    list_expression[len(list_expression)-1] = list_expression[len(list_expression)-1] + n
                else:
                    list_expression.append(n)
            else:
                list_expression.append(n)
        return list_expression
    
    def priority(op1,op2):
        p_op1=Utils.priority_operators(op1)
        p_op2=Utils.priority_operators(op2)
        if p_op1 > p_op2:
            return True
        return False

    def priority_operators(op):
        if op in ["+","-"]:
            return 1
        elif op in ["/","*"]:
            return 2
        else: 
            return 0

    def resolve_math(operand1,operand2,operator):
        if operator == '/':
            return operand2/operand1
        elif operator == '*':
            return operand2*operand1
        elif operator == '+':
            return operand2+operand1
        elif operator == '-':
            return operand2-operand1
        else:
            raise Exception("ERROR: Operator not valid")