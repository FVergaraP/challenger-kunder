import sys, os, traceback
from utils.utils import Utils

class Calculator:

    output_queue = []
    operator_stack = []

    def evaluate(n_expression):
        postfix_notation = Calculator.get_postfix_notation(n_expression)
        result = Calculator.resolve_postfix_notation(postfix_notation)
        return result

    
    def resolve_postfix_notation(postfix_notation):
        result_stack = []
        index_postfix = 0
        while len(postfix_notation) > index_postfix:
            if postfix_notation[index_postfix].isnumeric():
                result_stack.insert(0,int(postfix_notation[index_postfix]))
            else:
                operand1 = result_stack.pop(0)
                operand2 = result_stack.pop(0)
                result = Utils.resolve_math(operand1,operand2,postfix_notation[index_postfix])
                result_stack.insert(0,result)
            index_postfix = index_postfix + 1

        return result_stack[0]

    def get_postfix_notation(n_expression):
        list_expression = Utils.expression_to_list(n_expression)
        for element in list_expression:

            if element.isnumeric():
                Calculator.output_queue.append(element)
            if element in ['{','(','[']:
                Calculator.operator_stack.insert(0,element)
            if element in ['}',')',']']:
                len_operator_stack = len(Calculator.operator_stack)
                index_stack = 0
                while len(Calculator.operator_stack) > 0:
                    operator_removed = Calculator.operator_stack.pop(0)
                    if operator_removed == Utils.get_oposite_brack(element):
                        break
                    else:
                        Calculator.output_queue.append(operator_removed)
                    index_stack = index_stack + 1
            if element in ['+','-','*','/']:
                inserted = False
                while not inserted:
                    if len(Calculator.operator_stack) == 0:
                        Calculator.operator_stack.insert(0,element)
                        inserted = True
                    else:                        
                        if Utils.priority(element,Calculator.operator_stack[0]):
                            Calculator.operator_stack.insert(0,element)
                            inserted = True
                        else:
                            operator_removed = Calculator.operator_stack.pop(0)
                            Calculator.output_queue.append(operator_removed)
        
        while len(Calculator.operator_stack) > 0:
            operator_removed =  Calculator.operator_stack.pop(0)
            if operator_removed in ['{','(','[','}',')',']']:
                raise Exception("ERROR: Syntaxis Error")
            Calculator.output_queue.append(operator_removed)
        return Calculator.output_queue
    


def main():
    try:
        Utils.validate_args(sys.argv)

        path_expression = sys.argv[1]
        file_expression = open(path_expression,"r")
        numerical_expression = file_expression.read()
        print(Calculator.evaluate(numerical_expression))
    except Exception as e:
        print(e)

main()