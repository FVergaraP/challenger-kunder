# Desafío 1 Kunder
 
Primer desafío para el proceso de postulación a desarrollador fullstack. Que consiste en una calculadora de expresiones numéricas.
 
## Comenzando 🚀
 
Para realizar la revisión es necesario bajar el proyecto
 
```
git clone https://FVergaraP@bitbucket.org/FVergaraP/challenger-kunder.git
```
 
## Requisitos/Condiciones 📋
 
*Para asegurar una correcta ejecución se recomienda el uso de Python3.7
 
 
## Ejecución 🔧
 
Para obtener el resultado se debe ejecutar el scpryt numericalExpressionCalculator.py y pasar como argumento el path absoluto del fichero .txt donde se encuentra la expresión numérica.
Dentro del proyecto se deja un ejemplo en el directorio /challenge1/challenge/testFiles/expresionNumerica1.txt
 
```
PATH/PYTHON/python3.7 /PATH/PROYECTO/numericalExpressionCalculator.py /PATH/PROYECTO/DesafioKunder/challenge1/challenge/testFiles/expresionNumerica1.txt
```
 
## Construido con 🛠️
 
* Python 3.7
* VisualStudioCode
 
 
## Autor ✒️
 
**Francisco Vergara Peña** - *Desarrollador*
 

